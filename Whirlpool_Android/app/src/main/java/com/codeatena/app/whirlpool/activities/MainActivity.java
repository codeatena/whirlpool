package com.codeatena.app.whirlpool.activities;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.codeatena.app.whirlpool.R;
import com.codeatena.app.whirlpool.fragments.LoadFragment;
import com.codeatena.app.whirlpool.fragments.WashFragment;
import com.codeatena.lib.utility.FragmentUtility;

public class MainActivity extends Activity {

    LinearLayout llytContent;
    WashFragment washFragment;
    LoadFragment loadFragment;

    RelativeLayout rlytVideo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initWidget();
        initValue();
        initEvent();
    }

    private void initWidget() {
        llytContent = (LinearLayout) findViewById(R.id.content_linearLayout);
        rlytVideo = (RelativeLayout) findViewById(R.id.video_relativeLayout);
    }

    private void initValue() {

        washFragment = new WashFragment();
        loadFragment = new LoadFragment();
        showWash();
    }

    private void initEvent() {
        rlytVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(MainActivity.this, VideoActivity.class), 1000);
            }
        });
    }

    public void showWash() {
        if (washFragment != null) {
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_up);
            transaction.replace(R.id.content_linearLayout, washFragment).commit();
        }
    }

    public void showLoad() {
        if (loadFragment != null) {
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
//            transaction.setCustomAnimations(R.anim.slide_in_down, R.anim.slide_out_down);
            transaction.replace(R.id.content_linearLayout, loadFragment).commit();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1000) {
            showWash();
            washFragment.initValue();
        }
    }
}