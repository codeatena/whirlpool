package com.codeatena.app.whirlpool.activities;

import android.app.Activity;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.VideoView;

import com.codeatena.app.whirlpool.R;

public class PlayActivity extends Activity {

    VideoView videoView;
    TextView xButtonTextView;

    public static int nVideoRes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);

        initWidget();
        initValue();
        initEvent();
    }

    private void initWidget() {
        videoView = (VideoView) findViewById(R.id.videoView);
        xButtonTextView = (TextView) findViewById(R.id.x_button_textView);
    }

    private void initValue() {
        videoView.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + nVideoRes));
        videoView.requestFocus();
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mp.start();
            }
        });
        videoView.start();
    }

    private void initEvent() {
        xButtonTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}