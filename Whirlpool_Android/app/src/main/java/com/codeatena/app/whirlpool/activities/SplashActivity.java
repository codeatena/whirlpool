package com.codeatena.app.whirlpool.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.codeatena.app.whirlpool.R;

import java.util.Timer;
import java.util.TimerTask;

public class SplashActivity extends Activity {

    final int SPLASH_TIME = 500;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        initWidget();
        initValue();
        initEvent();
    }

    private void initWidget() {

    }

    private void initValue() {

    }

    private void initEvent() {
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                // this code will be executed after 2 seconds
                startActivity(new Intent(SplashActivity.this, StartActivity.class));
                finish();
            }
        }, SPLASH_TIME);
    }
}