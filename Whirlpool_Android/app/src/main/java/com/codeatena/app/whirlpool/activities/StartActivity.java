package com.codeatena.app.whirlpool.activities;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.codeatena.app.whirlpool.R;

import java.util.Timer;
import java.util.TimerTask;

public class StartActivity extends Activity {

    final String TAG = "StartActivity";
    final int DURATION_TIME_OPUT = 2 * 60 * 1000;

    RelativeLayout rlRoot;
    TextView tvBeginButton;
    VideoView videoView;
    RelativeLayout rlInstruction;

    Timer timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        initWidget();
        initValue();
        initEvent();
    }

    private void initWidget() {
        rlRoot = (RelativeLayout) findViewById(R.id.root_relativeLayout);
        tvBeginButton = (TextView) findViewById(R.id.begin_button_textView);
        videoView = (VideoView) findViewById(R.id.videoView);
        rlInstruction = (RelativeLayout) findViewById(R.id.instruction_relativeLayout);
    }

    private void initValue() {
        rlInstruction.setVisibility(View.GONE);
        initVideo();
        initInstructionTimer();
    }

    private void initEvent() {
        tvBeginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(StartActivity.this, MainActivity.class));
                finish();
            }
        });
        rlRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG, "rlRoot clicked");
                rlInstruction.setVisibility(View.VISIBLE);
                initInstructionTimer();
            }
        });
        rlInstruction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG, "rlInstruction clicked");
                rlInstruction.setVisibility(View.GONE);
                initInstructionTimer();
            }
        });
    }

    private void initInstructionTimer() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        rlInstruction.setVisibility(View.VISIBLE);
//                        initVideo();
                    }
                });
            }
        }, DURATION_TIME_OPUT);
    }

    private void initVideo() {
        videoView.setVisibility(View.VISIBLE);
        if (videoView.isPlaying()) {
            videoView.stopPlayback();
        }
        videoView.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.start));
        videoView.requestFocus();
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mp.start();
            }
        });
        videoView.start();
    }
}