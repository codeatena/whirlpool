package com.codeatena.app.whirlpool.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.codeatena.app.whirlpool.R;

public class VideoActivity extends Activity {

    TextView tvBackButton;

    ImageView imgPlayButton1;
    ImageView imgPlayButton2;
    ImageView imgPlayButton3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);

        initWidget();
        initValue();
        initEvent();
    }

    private void initWidget() {
        tvBackButton = (TextView) findViewById(R.id.back_button_textView);

        imgPlayButton1 = (ImageView) findViewById(R.id.play_button_imageView1);
        imgPlayButton2 = (ImageView) findViewById(R.id.play_button_imageView2);
        imgPlayButton3 = (ImageView) findViewById(R.id.play_button_imageView3);
    }

    private void initValue() {

    }

    private void initEvent() {
        tvBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        imgPlayButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PlayActivity.nVideoRes = R.raw.video1;
                startActivity(new Intent(VideoActivity.this, PlayActivity.class));
            }
        });
        imgPlayButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PlayActivity.nVideoRes = R.raw.video2;
                startActivity(new Intent(VideoActivity.this, PlayActivity.class));
            }
        });
        imgPlayButton3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PlayActivity.nVideoRes = R.raw.video3;
                startActivity(new Intent(VideoActivity.this, PlayActivity.class));
            }
        });
    }
}