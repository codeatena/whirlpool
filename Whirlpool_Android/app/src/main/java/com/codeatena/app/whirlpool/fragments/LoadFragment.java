package com.codeatena.app.whirlpool.fragments;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.VideoView;
import android.widget.ViewSwitcher;

import com.codeatena.app.whirlpool.R;
import com.codeatena.app.whirlpool.activities.MainActivity;
import com.codeatena.app.whirlpool.activities.StartActivity;
import com.codeatena.app.whirlpool.activities.VideoActivity;
import com.codeatena.listener.OnSwipeTouchListener;

import org.w3c.dom.Text;

import java.util.Timer;
import java.util.TimerTask;

public class LoadFragment extends Fragment {

    LinearLayout ltWhatToWash;
    VideoView videoView;
    LinearLayout ltBackground;

//    TextSwitcher tsTitle;
//    TextSwitcher tsDesc;
//
//    Timer timer;
//
//    final int CROSSFADE_TIME = 1500;
//
//    String[] strTitles;
//    String[] strDescs;
//
//    int nIndexCrossFade = 1;

//    public class CrossFadeTimerTask extends TimerTask {
//        @Override
//        public void run() {
//            getActivity().runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    updateTextView();
//                }
//            });
//        }
//    }

//    private ViewSwitcher.ViewFactory vfTitle = new ViewSwitcher.ViewFactory() {
//        @Override
//        public View makeView() {
//            TextView textView = new TextView(getActivity());
//            textView.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL);
//            textView.setTextSize(25);
//            textView.setTextColor(Color.WHITE);
//            return textView;
//        }
//    };
//
//    private ViewSwitcher.ViewFactory vfDesc = new ViewSwitcher.ViewFactory() {
//        @Override
//        public View makeView() {
//            TextView textView = new TextView(getActivity());
//            textView.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL);
//            textView.setTextSize(15);
//            textView.setTextColor(Color.WHITE);
//            return textView;
//        }
//    };

    public LoadFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_load, container, false);

        ltBackground = (LinearLayout) view.findViewById(R.id.background_linearLayout);
        videoView = (VideoView) view.findViewById(R.id.videoView);
        ltWhatToWash = (LinearLayout) view.findViewById(R.id.what_to_wash_linearLayout);

//        tsTitle = (TextSwitcher) view.findViewById(R.id.title_textSwitcher);
//        tsDesc = (TextSwitcher) view.findViewById(R.id.desc_textSwitcher);

        initValue();
        initEvent();

        return view;
    }

    private void initValue() {
        ltBackground.setVisibility(View.VISIBLE);
        initVideo();
//        initCrossFade();
    }

    private void initVideo() {

        videoView.setVideoURI(Uri.parse("android.resource://" + getActivity().getPackageName() + "/" + R.raw.load_go));
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mp.start();
            }
        });
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                ltBackground.setVisibility(View.GONE);
//                videoView.start();
            }
        });
        videoView.start();
//        videoView.setVisibility(View.GONE);
    }

//    private void initCrossFade() {
//
//        if (timer != null) {
//            timer.cancel();
//            timer = null;
//        }
//
//        timer = new Timer();
//        timer.schedule(new CrossFadeTimerTask(), 0, CROSSFADE_TIME);
//
//        strTitles = getResources().getStringArray(R.array.load_go_titles);
//        strDescs = getResources().getStringArray(R.array.load_go_descs);
//
//        tsTitle.setFactory(vfTitle);
//        tsDesc.setFactory(vfDesc);
//        Animation in = AnimationUtils.loadAnimation(getActivity(),
//                android.R.anim.fade_in);
//        Animation out = AnimationUtils.loadAnimation(getActivity(),
//                android.R.anim.fade_out);
//        tsTitle.setInAnimation(in);
//        tsDesc.setInAnimation(in);
//        tsTitle.setOutAnimation(out);
//        tsDesc.setOutAnimation(out);
//    }

    private void initEvent() {
        ltWhatToWash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity mainActivity = (MainActivity) getActivity();
                mainActivity.showWash();
            }
        });
        videoView.setOnTouchListener(new OnSwipeTouchListener(getActivity()) {
            public void onSwipeTop() {
                MainActivity mainActivity = (MainActivity) getActivity();
                mainActivity.showWash();
            }

            public void onSwipeRight() {
//                Toast.makeText(getActivity(), "right", Toast.LENGTH_SHORT).show();
            }

            public void onSwipeLeft() {
//                Toast.makeText(getActivity(), "left", Toast.LENGTH_SHORT).show();
            }

            public void onSwipeBottom() {
//                Toast.makeText(getActivity(), "bottom", Toast.LENGTH_SHORT).show();
            }
        });
    }

//    private void updateTextView() {
//        nIndexCrossFade = 1 - nIndexCrossFade;
//        tsTitle.setText(strTitles[nIndexCrossFade]);
//        tsDesc.setText(strDescs[nIndexCrossFade]);
//    }

//    @Override
//    public void onStop() {
//        if (timer != null) {
//            timer.cancel();
//            timer = null;
//        }
//        super.onStop();
//    }
}
