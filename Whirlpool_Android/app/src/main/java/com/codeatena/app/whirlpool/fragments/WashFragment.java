package com.codeatena.app.whirlpool.fragments;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.app.Fragment;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.codeatena.app.whirlpool.App;
import com.codeatena.app.whirlpool.Consts;
import com.codeatena.app.whirlpool.R;
import com.codeatena.app.whirlpool.activities.MainActivity;
import com.codeatena.app.whirlpool.activities.StartActivity;
import com.codeatena.listener.OnSwipeTouchListener;

import java.util.Timer;
import java.util.TimerTask;

public class WashFragment extends Fragment {

    RelativeLayout rlLoadGo;

    RelativeLayout rlMixed;
    RelativeLayout rlTowels;
    RelativeLayout rlDelicates;
    RelativeLayout rlActiveWear;

    RelativeLayout rlContent;

    VideoView videoView;

    ImageView ivMixed;
    ImageView ivTowels;
    ImageView ivDelicates;
    ImageView ivActiveWear;

    TextView tvExploreMoreButton;

    Consts.WASH_TYPE nWashType;

    ImageView ivDrawer;

    Timer timer;

    public WashFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_wash, container, false);

        rlLoadGo = (RelativeLayout) view.findViewById(R.id.load_go_relativeLayout);
        rlContent = (RelativeLayout) view.findViewById(R.id.content_relativeRayout);

        videoView = (VideoView) view.findViewById(R.id.videoView);

        rlMixed = (RelativeLayout) view.findViewById(R.id.mixed_relatvieLayout);
        rlTowels = (RelativeLayout) view.findViewById(R.id.towels_relatvieLayout);
        rlDelicates = (RelativeLayout) view.findViewById(R.id.delicates_relativeLayout);
        rlActiveWear = (RelativeLayout) view.findViewById(R.id.active_wear_relativeLayout);

        ivMixed = (ImageView) view.findViewById(R.id.mixed_imageView);
        ivTowels = (ImageView) view.findViewById(R.id.towels_imageView);
        ivDelicates = (ImageView) view.findViewById(R.id.delicates_imageView);
        ivActiveWear = (ImageView) view.findViewById(R.id.active_wear_imageView);

        ivDrawer = (ImageView) view.findViewById(R.id.drawer_imageView);
        tvExploreMoreButton = (TextView) view.findViewById(R.id.explore_more_button_textView);

        initValue();
        initEvent();

        return view;
    }

    public void initValue() {
        if (videoView.isPlaying()) {
            videoView.stopPlayback();
        }
        videoView.setVisibility(View.GONE);
        ivDrawer.setVisibility(View.GONE);
        tvExploreMoreButton.setVisibility(View.GONE);
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    private void openDrawer() {
        ivDrawer.setVisibility(View.VISIBLE);
        rlLoadGo.setVisibility(View.GONE);

        ObjectAnimator transAnimation= ObjectAnimator.ofFloat(ivDrawer, "y", -120, 0);
        transAnimation.setDuration(1000);//set duration
        transAnimation.start();

        transAnimation.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                MainActivity mainActivity = (MainActivity) getActivity();
                if (mainActivity != null) {
                    mainActivity.showLoad();
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    private void initEvent() {
//        rlLoadGo.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                openDrawer();
//            }
//        });
        rlLoadGo.setOnTouchListener(new OnSwipeTouchListener(getActivity()) {
            public void onSwipeTop() {

            }

            public void onSwipeRight() {
//                Toast.makeText(getActivity(), "right", Toast.LENGTH_SHORT).show();
            }

            public void onSwipeLeft() {
//                Toast.makeText(getActivity(), "left", Toast.LENGTH_SHORT).show();
            }

            public void onSwipeBottom() {
//                Toast.makeText(getActivity(), "bottom", Toast.LENGTH_SHORT).show();
                openDrawer();
            }
        });
        rlMixed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressWash(Consts.WASH_TYPE.MIXED);
            }
        });
        rlTowels.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressWash(Consts.WASH_TYPE.TOWELS);
            }
        });
        rlDelicates.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressWash(Consts.WASH_TYPE.DELICATES);
            }
        });
        rlActiveWear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressWash(Consts.WASH_TYPE.ACTIVE_WEAR);
            }
        });

        ivMixed.setOnLongClickListener(new MyOnLongClickListener());
        ivTowels.setOnLongClickListener(new MyOnLongClickListener());
        ivDelicates.setOnLongClickListener(new MyOnLongClickListener());
        ivActiveWear.setOnLongClickListener(new MyOnLongClickListener());

        rlContent.setOnDragListener(new MyDragListener());
    }

    private void progressWash(final Consts.WASH_TYPE washType) {
        nWashType = washType;

        if (videoView.isPlaying()) {
            videoView.stopPlayback();
        }
        videoView.setVisibility(View.GONE);

        String path = "android.resource://" + getActivity().getPackageName() + "/";

        if (nWashType == Consts.WASH_TYPE.MIXED) {
            path += R.raw.mixed;
        }
        if (nWashType == Consts.WASH_TYPE.TOWELS) {
            path += R.raw.towels;
        }
        if (nWashType == Consts.WASH_TYPE.DELICATES) {
            path += R.raw.delicates;
        }
        if (nWashType == Consts.WASH_TYPE.ACTIVE_WEAR) {
            path += R.raw.active_wear;
        }

        if (videoView.isPlaying()) {
            videoView.stopPlayback();
        }

        videoView.setVisibility(View.VISIBLE);
        videoView.setVideoURI(Uri.parse(path));
        videoView.requestFocus();
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                initValue();
            }
        });
        videoView.start();

        tvExploreMoreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initValue();
            }
        });

        if (timer != null) {
            timer.cancel();
            timer = null;
        }

        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        tvExploreMoreButton.setVisibility(View.VISIBLE);
                    }
                });
            }
        }, 20000);
    }

    private class MyOnLongClickListener implements View.OnLongClickListener {
        public boolean onLongClick(View view) {
            ClipData data = ClipData.newPlainText("", "");

            MyDragShadowBuilder shadowBuilder = new MyDragShadowBuilder(view);
            view.startDrag(data, shadowBuilder, view, 0);
            return true;
        }
    };

    private static class MyDragShadowBuilder extends View.DragShadowBuilder {

        // The drag shadow image, defined as a drawable thing
        private int width, height;

        // Defines the constructor for myDragShadowBuilder
        public MyDragShadowBuilder(View v) {

            // Stores the View parameter passed to myDragShadowBuilder.
            super(v);
            // Creates a draggable image that will fill the Canvas provided by the system.
        }

        // Defines a callback that sends the drag shadow dimensions and touch point back to the
        // system.
         //
        @Override
        public void onProvideShadowMetrics (Point size, Point touch) {
            super.onProvideShadowMetrics(
                    size,
                    touch);

            size.set(size.x * 2, size.y * 2);
            touch.set(touch.x * 2, touch.y * 2);
        }

        @Override
        public void onDrawShadow(Canvas canvas) {
            canvas.scale(2, 2);
            getView().draw(canvas);
        }
    }

    private final class MyTouchListener implements View.OnTouchListener {
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                ClipData data = ClipData.newPlainText("", "");

                View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
                view.startDrag(data, shadowBuilder, view, 0) ;
//                view.setVisibility(View.INVISIBLE);
                return true;
            } else {
                return false;
            }
        }
    }

    class MyDragListener implements View.OnDragListener {
        @Override
        public boolean onDrag(View v, DragEvent event) {
            int action = event.getAction();
            switch (event.getAction()) {
                case DragEvent.ACTION_DRAG_STARTED:
                    // do nothing
                    Log.e("WashFragment", "ACTION_DRAG_STARTED");
                    break;
                case DragEvent.ACTION_DRAG_ENTERED:
//                    v.setBackgroundResource(R.drawable.mixed);
                    Log.e("WashFragment", "ACTION_DRAG_ENTERED");
                    break;
                case DragEvent.ACTION_DRAG_EXITED:
//                    v.setBackgroundResource(R.drawable.mixed);
                    Log.e("WashFragment", "ACTION_DRAG_EXITED");
                    break;
                case DragEvent.ACTION_DROP:
                    // Dropped, reassign View to ViewGroup
                    View view = (View) event.getLocalState();
                    if (view == ivMixed) {
                        Log.e("WashFragment", "Mixed");
                        progressWash(Consts.WASH_TYPE.MIXED);
                    }
                    if (view == ivTowels) {
                        Log.e("WashFragment", "Towels");
                        progressWash(Consts.WASH_TYPE.TOWELS);
                    }
                    if (view == ivDelicates) {
                        Log.e("WashFragment", "Delicates");
                        progressWash(Consts.WASH_TYPE.DELICATES);
                    }
                    if (view == ivActiveWear) {
                        Log.e("WashFragment", "Active Wear");
                        progressWash(Consts.WASH_TYPE.ACTIVE_WEAR);
                    }
//                    ViewGroup owner = (ViewGroup) view.getParent();
//                    owner.removeView(view);
//                    LinearLayout container = (LinearLayout) v;
//                    container.addView(view);
//                    view.setVisibility(View.VISIBLE);
                    Log.e("WashFragment", "ACTION_DROP");
                    break;
                case DragEvent.ACTION_DRAG_ENDED:
//                    v.setBackgroundResource(R.drawable.mixed);
                    Log.e("WashFragment", "ACTION_DRAG_ENDED");
                default:
                    break;
            }
            return true;
        }
    }
}