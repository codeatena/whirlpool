package com.codeatena.lib.utility;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;

/**
 * Created by User on 11/25/2015.
 */
public class FragmentUtility {

    public static FragmentUtility instance = null;

    public static FragmentUtility getInstance() {

        if (instance == null) {
            instance = new FragmentUtility();
        }
        return instance;
    }

    public void showFragment(Activity activity, Fragment fragment, boolean animation) {
        FragmentTransaction ft = activity.getFragmentManager().beginTransaction();

        if (animation) {
            ft.setCustomAnimations(android.R.animator.fade_in,
                    android.R.animator.fade_out);
        }
        if (fragment.isHidden()) {
            ft.show(fragment);
        } else {

        }

        ft.commit();
    }

    public void hideFragment(Activity activity, Fragment fragment, boolean animation) {
        FragmentTransaction ft = activity.getFragmentManager().beginTransaction();

        if (animation) {
            ft.setCustomAnimations(android.R.animator.fade_in,
                    android.R.animator.fade_out);
        }

        if (fragment.isHidden()) {

        } else {
            ft.hide(fragment);
        }

        ft.commit();
    }
}
